# Challenge - Classes Exercise

# Add a method to the Car class called age
# that returns how old the car is (2019 - year)

# *Be sure to return the age, not print it

class Car:

    def __init__(self, year, make, model):
        self.year = year
        self.make = make
        self.model = model

    def age(self):
        return self.year - self.make


myCar = Car(2022, 2019, "Ford Fusion")
print("The age of the car is {} years".format(myCar.age()))
